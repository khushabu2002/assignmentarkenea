import { Component, OnInit } from '@angular/core';
import { User } from '../users/users.component';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  users: User[] = [];
  constructor() { }

  ngOnInit(): void {
  }

  setUsers(user) {
    this.users = user;
  }

}
