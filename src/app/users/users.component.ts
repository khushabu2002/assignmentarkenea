import { AfterViewInit, Component, EventEmitter, OnInit, Output, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MatDialog } from '@angular/material/dialog';
import { EditComponent } from '../edit/edit.component';
import { DeleteComponent } from '../delete/delete.component';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatTable, MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';


export interface User {
  name: string;
  email: string;
  gender: string;
  address: string;
  dateOfBirth: number;
}

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements AfterViewInit {
  @ViewChild(MatTable) table: MatTable<User[]>;
  displayedColumns: string[] = ['checkbox', 'name', 'email', 'gender', 'address', 'dateOfBirth', 'edit', 'delete'];
  userslist: User[] = [];
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @Output() setUsers = new EventEmitter();
  dataSource: MatTableDataSource<User>;
  constructor(private http: HttpClient, public dialogController: MatDialog, private formBuilder: FormBuilder) {
  }

  ngAfterViewInit(): void {
    this.getUsersList();
  }

  getUsersList(): void {
    this.http.get('../../assets/users.json').subscribe((userList: User[]) => {
      this.userslist = userList;
      this.dataSource = new MatTableDataSource(this.userslist);
      this.dataSource.paginator = this.paginator;
      this.setUsers.emit(this.userslist);
    });
  }

  edit(user: User, index: number): void {

    const userForm = this.formBuilder.group({
      name: user.name,
      address: user.address,
      email: user.email,
      dateOfBirth: new Date(user.dateOfBirth),
      gender: user.gender
    });
    const editDialog = this.dialogController.open(EditComponent, {
      data: userForm,
      width: '700px',
    });

    editDialog.afterClosed().subscribe((result: FormGroup) => {
      if (result) {
        this.userslist[index] = result.getRawValue();
        this.userslist[index].dateOfBirth = new Date(result.get('dateOfBirth').value).getTime();
        this.dataSource = new MatTableDataSource(this.userslist);
        this.dataSource.paginator = this.paginator;
        this.table.renderRows();
        this.setUsers.emit(this.userslist);
      }
    });
  }

  delete(user: User, index: number): void {
    const deleteDialog = this.dialogController.open(DeleteComponent, {
      data: user,
      width: '700px'
    });

    deleteDialog.afterClosed().subscribe(result => {
      if (result) {
        this.userslist.splice(index, 1);
        this.userslist = JSON.parse(JSON.stringify(this.userslist));
        this.dataSource = new MatTableDataSource(this.userslist);
        this.dataSource.paginator = this.paginator;
        this.table.renderRows();
        this.setUsers.emit(this.userslist);
      }
    });
  }


}
