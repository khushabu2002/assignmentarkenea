import { Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { User } from '../users/users.component';

@Component({
  selector: 'app-progressbar',
  templateUrl: './progressbar.component.html',
  styleUrls: ['./progressbar.component.scss']
})
export class ProgressbarComponent implements OnChanges {

  @Input() users: User[] = [];
  value: number = 0;
  maleUSers: number = 0;
  femaleUsers: number = 0;
  constructor() { }

  ngOnChanges(changes: SimpleChanges): void {
    this.maleUSers = this.users.filter(user => user.gender === 'Male').length;
    this.femaleUsers = this.users.length - this.maleUSers;
    this.value = this.maleUSers * 100 / this.users.length;
  }



}
